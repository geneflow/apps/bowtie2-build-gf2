Bowtie2 Build GeneFlow App
==========================

Version: 2.4.1-01

This GeneFlow2 app wraps the Bowtie2 Build tool.

Inputs
------

1. reference: Reference Sequence FASTA - A reference sequence in FASTA file format to be indexed for Bowtie2. 

Parameters
----------

1. threads: CPU Threads - Number of CPU threads used for building index. Default: 2

2. output: Output Directory - The directory at which the new BWA index will be generated. The default value for the output directory is "reference". This directory is relative to the workflow or step output location. 
